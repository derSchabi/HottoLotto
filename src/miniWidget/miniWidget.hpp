 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * miniWidget.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MINIWIDGET_H_
#define MINIWIDGET_H_

#include <QWidget>

class MiniWidget
	: public QWidget
{
	Q_OBJECT
public:
	MiniWidget(QWidget *parent = 0);
signals:
	void clicked();
public slots:
	void setFullscreen(bool);
private:
	void mousePressEvent(QMouseEvent *);
	
	void paintEvent(QPaintEvent *);	

	bool fullScreened;
	
};


inline void MiniWidget::setFullscreen(bool v)
{
    fullScreened = v;
}

#endif
