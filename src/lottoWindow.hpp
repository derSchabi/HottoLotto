 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * lottoWindow.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOTTOWINDOW_H_
#define LOTTOWINDOW_H_

#include <QWidget>
#include <QVector>
#include "coil/doubleCoil.hpp"
#include "table/table.hpp"
#include "closeWidget/closeWidget.hpp"
#include "miniWidget/miniWidget.hpp"
#include "revindButton/revindButton.hpp"

class QPushButton;
class QPaintEvent;
class MainWidget;

class LottoWindow
	: public QWidget
{
	Q_OBJECT
public:
    LottoWindow(QWidget *parent = 0);

    bool wasResetingCoils();
    bool isReady() const;
    void setSSS(int, int, int);      //speed, speedUpTime, spinTime
signals:
	void actionKeyPressed();
	void escapePressed();
    void coilClicked();
    void ready();
public slots:
    void resetCoils();
private slots:
    void onCoil();
    void onCoilReady();
private:

	void paintEvent(QPaintEvent *);
	void keyPressEvent(QKeyEvent *);
	void closeEvent(QCloseEvent *);

	CloseWidget *closeButton;	
	MiniWidget *miniButton;
    RevindButton *revindButton;

    QVector<DoubleCoil *>  coil;
		
    QVector<Table *> table;

    bool wasReset;

    int speed;
    int spinTime;
    int speedUpTime;

    friend class Core;
};	

#endif
