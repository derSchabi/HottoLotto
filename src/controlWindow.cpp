 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * controlWindow.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controlWindow.h"
#include "ui_controlWindow.h"
#include "settings/settingsDialog.hpp"
#include <QMenu>
#include <cstdlib>
#include <QMessageBox>
#include "global.hpp"

ControlWindow::ControlWindow(QWidget *parent)
    :QDialog(parent)
    ,ui(new Ui::ControlWindow)
{
    ui->setupUi(this);
    setWindowTitle(QString("HottoLotto ") + QString(version));
    setFixedSize(300, 230);
    QMenu *mainMenu = new QMenu();
    mainMenu->addAction(
                ui->actionNew);
    mainMenu->addAction(
                ui->actionFullscreen);
    mainMenu->addAction(
                ui->actionInfo);
    mainMenu->addAction(
                ui->actionSettings);

    mainMenu->addAction(
                ui->actionQuit);

    ui->nextButton->hide();
    ui->cb1->hide();
    ui->cb2->hide();
    ui->cb3->hide();
    ui->quitButton->hide();
    ui->resetButton->hide();

    ui->menuButton->setMenu(mainMenu);

}

ControlWindow::~ControlWindow()
{
    delete ui;
}

void ControlWindow::onSettings()
{
    emit settingsClicked();
}

void ControlWindow::onInfo()
{
    QMessageBox::about(this, QString(QString(tr("Info about HottoLotto ")) + QString(version)),
        tr("<p><b>HottoLotto")+QString(version)+
        tr("</b> is a lotto/bingo program for the use of public lotto/bingo events. "
        "It is written in C++ using the Qt5 framework.</p>"
        "<p>If you find any bugs want any features or have questions don´t mind writting a mail to "
        "<a href=\"mailto:chris.schabesberger@gmail.com\">chris.schabesberger@gmail.com</a></p>"
        "<p>Copyright &copy; 2013 Christian Schabesberger &lt;chris.schabesberger@gmail.com&gt;</p>"
        "<p>This program is free software: you can redistribute it and/or modify "
        "it under the terms of the GNU General Public License as published by "
        "the Free Software Foundation, either version 3 of the License, or "
        "(at your option) any later version.</p>"
        "<p>This program is distributed in the hope that it will be useful, "
        "but WITHOUT ANY WARRANTY; without even the implied warranty of "
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the "
        "GNU General Public License for more details.</p>"
        "You should have received a copy of the GNU General Public License "
        "along with this program. "
        "If not, see <a href=\"http://www.gnu.org/licenses/gpl-3.0\">http://www.gnu.org/licenses</a>.</p>"
        "<p>-----------</p><p><a href=\"http://qt-project.org\">Qt-Project</a></p>"
        "<p><a href=\"http://qt.digia.com/Product/Licensing/\">Qt-Licencing</a></p>"
        "<p><a href=\"http://www.randomnumbers.info/\">randomnumbers.info</a></p>"
        "<p>Source Code: <a href=\"http://github.com/theScrabi/HottoLotto\">http://github.com/theScrabi/HottoLotto</a></p>"));
}

void ControlWindow::onQuit()
{
    app->quit();
}

void ControlWindow::onReset()
{
    emit resetClicked();
}

void ControlWindow::onFullscreen()
{
    emit fullscreenClicked();
}

void ControlWindow::onCoilButton()
{
    emit coilButtonClicked();
}

void ControlWindow::onNextButton()
{
    emit nextButtonClicked();
}

void ControlWindow::onStartButton()
{
    emit startButtonClicked();
}

void ControlWindow::closeEvent(QCloseEvent *)
{
    onQuit();
}

void ControlWindow::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::LanguageChange)
        ui->retranslateUi(this);
    else
        QDialog::changeEvent(e);
}
