 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * lottoWindow.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lottoWindow.hpp"
#include "coil/doubleCoil.hpp"
#include "table/table.hpp"
#include "closeWidget/closeWidget.hpp"
#include "miniWidget/miniWidget.hpp"
#include "settings/settings.hpp"
#include "global.hpp"
#include <QtWidgets>
#include <iostream>

extern QApplication *app;

LottoWindow::LottoWindow(QWidget *parent)
    :QWidget(parent)
    ,closeButton(new CloseWidget())
    ,miniButton(new MiniWidget())
    ,revindButton(new RevindButton())
    ,wasReset(false)
{
    setWindowTitle(QString("Hotto Lotto ") + QString(version));
    setMinimumSize(800, 600);
    setWindowIcon(QIcon(":images/icon.png"));

    speed = settings->getSpeed();
    spinTime = settings->getSpinTime();
    speedUpTime = settings->getSpeedUpTime();

    //setup regulare

    revindButton->hide();

    QHBoxLayout *buttonLayout = new QHBoxLayout();
    buttonLayout->setAlignment(Qt::AlignAbsolute);
    buttonLayout->addWidget(closeButton);
    buttonLayout->addStretch();
    buttonLayout->addWidget(revindButton);
    buttonLayout->addStretch();
#ifndef __APPLE__        //using Mac's native fullscreen Button
                         //but requires minimal Mac OS X 10.7
    buttonLayout->addWidget(miniButton);
#else
    miniButton->setVisible(false);
#endif

    connect(closeButton, SIGNAL(clicked()),
            app, SLOT(quit()));

     //setup with loop

    QHBoxLayout *coilLayout = new QHBoxLayout();
    QHBoxLayout *tableLayout = new QHBoxLayout();

    for(int i = 0; i < 3; i++)
    {
        coil.push_back(new DoubleCoil(this));
        table.push_back(new Table(this));

        coil[i]->setSpeed(speed);
        coil[i]->setSpinTime(spinTime);
        coil[i]->setSpeedUpTime(speedUpTime);
        coil[i]->setSmallDigit("00");
        coil[i]->setBigDigit("29");
        coil[i]->setClickable(true);

        coilLayout->addWidget(coil[i]);
        tableLayout->addWidget(table[i]);

        connect(coil[i], SIGNAL(clicked()),
                this, SLOT(onCoil()));
        connect(coil[i], SIGNAL(ready()),
                this, SLOT(onCoilReady()));
    }

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(buttonLayout);
    mainLayout->addLayout(coilLayout);
    mainLayout->addLayout(tableLayout);


    setLayout(mainLayout);
}

void LottoWindow::paintEvent(QPaintEvent *)
{
	//mk background black
	QPainter painter(this);
	painter.setViewport(0, 0, width(), height());
	painter.setWindow(0, 0, width(), height());
	
	painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
	painter.setPen(QPen(Qt::black));
	painter.drawRect(0, 0, width(), height());
}

void LottoWindow::keyPressEvent(QKeyEvent *e)
{
	std::cout<<"Key Pressed: "<<e->key()<<std::endl;
	if(e->key() == Qt::Key_Space || e->key() == Qt::Key_Enter)
		emit actionKeyPressed();
	if(e->key() == Qt::Key_Escape)
		emit escapePressed();	
}

void LottoWindow::closeEvent(QCloseEvent *)
{
	app->quit();
}

void LottoWindow::onCoil()
{
    emit coilClicked();
}

void LottoWindow::onCoilReady()
{
    for(int i = 0; i < 3; i++)
        if(!coil[i]->isReady())
            return;

    for(int i = 0; i < 3; i++)
    {
        coil[i]->setSpeed(speed);
        coil[i]->setSpeedUpTime(speedUpTime);
        coil[i]->setSpinTime(spinTime);
    }
    emit ready();
}

bool LottoWindow::isReady() const
{
    for(int i = 0; i < 3; i++)
        if(!coil[i]->isReady())
            return false;
    return true;
}

void LottoWindow::resetCoils()
{
    for(int i = 0; i < 3; i++)
    {
        coil[i]->setSpeedUpTime(0);
        coil[i]->setSpinTime(0);
        coil[i]->moveTo(0);
    }
    wasReset = true;
}

bool LottoWindow::wasResetingCoils()
{
    bool v = wasReset;
    wasReset = false;
    return v;
}

void LottoWindow::setSSS(int s, int sut, int st)
{
    speed = s;
    speedUpTime = sut;
    spinTime = st;
    for(int i = 0; i < 3; i++)
    {
        coil[i]->setSpeed(speed);
        coil[i]->setSpeedUpTime(speedUpTime);
        coil[i]->setSpinTime(spinTime);
    }
}
