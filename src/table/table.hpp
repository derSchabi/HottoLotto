 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * table.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TABLE_H_
#define TABLE_H_

#include <QWidget>
#include <QStringList>


class QTimer;
class QTime;
class QPaintEvent;

class Table
	: public QWidget
{
	Q_OBJECT
public:
    Table(QWidget *parent = 0);
	void addText(QString);
	void reset();
	bool isReady() const;
signals:
	void main_loopRevind();
	void ready();
private slots:
	void main_loop();
private:
	static const int minSize = 10;
	static const float xWindowSize = 400;
	
	static const int speed = 500; 		//in px/s

	static const int stdXFontPos = 10;

	static const int fontSpacing = 10;		//distence betwen font lines

	static const int fontSize = 50;

	int yFontPos;
	int yPosToReach;
	QStringList *storedLines;	

	void paintEvent(QPaintEvent *);
	void drawFrame(int);
	
	QTimer *loopBrake;
	QTime *frameTime;
	
	bool resetting;
};

inline void Table::reset()
{
	resetting = true;
	delete storedLines;
	storedLines = new QStringList();
	repaint();
}

inline bool Table::isReady() const
{
    return (yFontPos == yPosToReach);
}


#endif
