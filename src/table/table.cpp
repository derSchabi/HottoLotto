 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * table.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "table.hpp"
#include <QtWidgets>
#include <iostream>
#include <global.hpp>

Table::Table(QWidget *parent)
    : QWidget(parent)
    , storedLines(new QStringList())
    , loopBrake(new QTimer())
    , frameTime(new QTime())
    , resetting(false)
{
	setMinimumSize(10, 4*10);
	connect(loopBrake, SIGNAL(timeout()),
		this, SLOT(main_loop()));
	connect(this, SIGNAL(main_loopRevind()),
		this, SLOT(main_loop()));
}

void Table::addText(QString tda)
{
	frameTime->start();
	storedLines->push_back(tda);
	yFontPos = (xWindowSize*height())/width();
	yPosToReach = 
		storedLines->size()*(fontSize+fontSpacing);
	main_loop();
}

void Table::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setViewport(0, 0, width(), height());


			//draw background
	painter.setWindow(0, 0, width(), height());
	painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
	painter.setPen(QPen(Qt::black));
	painter.drawRect(0, 0, width(), height());

			//leave all black if reset is done
	if(resetting) 
	{
		resetting = false;
		return;
	}
			//draw Text
	painter.setWindow(0, 0, xWindowSize, (xWindowSize*height())/width());
	painter.setPen(QPen(Qt::white, fontSize));
	painter.setFont(QFont("ExtraBold", fontSize));
			
	int position = 0;
	for(int i = 0; i < storedLines->size()-1; i++)
	{
		position += fontSize + fontSpacing;
		painter.drawText(stdXFontPos, position, storedLines->at(i));
	}
			//draw animated text
    if(storedLines->size())
        painter.drawText(stdXFontPos, yFontPos, storedLines->at(
                            storedLines->size()-1));
}

void Table::main_loop()
{
	if(!isReady())
	{
		int ft = frameTime->elapsed();
		if(ft < 1000/60)
		{
			loopBrake->stop();
			loopBrake->start((1000/60)-ft);
			return;
		}
		frameTime->restart();
		drawFrame(ft);
		if(!isReady())
			emit main_loopRevind();
	}
	else
	{
		loopBrake->stop();
		emit ready();
	}
}

void Table::drawFrame(int tslf)
{	
	if(yFontPos > yPosToReach)
		yFontPos -= (speed*tslf/1000);	
	else
		yFontPos = yPosToReach;
	repaint();
}
