#include "revindButton.hpp"
#include <QtWidgets>

RevindButton::RevindButton(QWidget *parent)
	:QWidget(parent)
    ,pixmap(":/images/revind.png")
{
	setMinimumSize(20, 20);
	setMaximumSize(20, 20);
}

void RevindButton::mousePressEvent(QMouseEvent *)
{
	emit clicked();
}

void RevindButton::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setWindow(0, 0, width(), height());
	painter.setViewport(0, 0, width(), height());	
	
	painter.drawPixmap(0, 0, pixmap);
}
