#ifndef REVINDBUTTON_H_
#define REVINDBUTTON_H_

#include <QWidget>
#include <QPixmap>

class RevindButton
	: public QWidget
{
	Q_OBJECT
public:
	RevindButton(QWidget *parent = 0);
signals:
	void clicked();
private:
	void paintEvent(QPaintEvent *);
	void mousePressEvent(QMouseEvent *);

	QPixmap pixmap;
};

#endif
