 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * doubleCoil.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DOUBLECOIL_H_
#define DOUBLECOIL_H_

#include <QWidget>
#include "singleCoil.hpp"

class QPaintEvent;
class QResizeEvent;
class QPixmap;

class DoubleCoil
	: public QWidget
{
	Q_OBJECT
public:
    DoubleCoil(QWidget *parent = 0);
	
    int getValue() const;
    float getSpeed() const;
    int getSpinTime() const;
    int getSpeedUpTime() const;
    bool isReady() const;
    bool isMovDigWnEqual() const;

    void setSpeed(float);
    void setSpinTime(int);              //in milliseconds !!!
    void setSmallDigit(QString);        //enter two characters, once for each coil
    void setBigDigit(QString);          // -""-
    void setSpeedUpTime(int);           //in milliseconds !!!
    void setMovDigWnEqual(bool);
public slots:
    void setClickable(bool);            //emits clicked() if true
                                        //and widget was clicked

                                        //do not call the following two commands
                                        //before setting any properties

    void setTo(int);                    //do not call while coil is spinning
    void moveTo(int);
signals:
	void clicked();
	void main_loopRevind();
	void ready();
private slots:
	void main_loop();
private:

	static const float stdWidth = 432;
	static const float stdHeight = 332;
	
	static const float stdSCoilWidth = 200;
	static const float stdSCoilHeight = 300;

    static const int lineSize = 22;	//frame thicknes

    float sizeCoefficient;          //neaded to size the single coil widgets
                                    //that the bitmap witch they produce fit into
                                    //this one

	void paintEvent(QPaintEvent *);	
	void resizeEvent(QResizeEvent *);
	void mousePressEvent(QMouseEvent *); 

    void setSC();           //set sizeCoefficient;
	double bySC(double);	//multiplied by sizeCoefficient
    double dbC(double);     //divided by sizeCoefficient
	
	int containingNumber;
    QString smallestDigit;
    QString biggestDigit;
    int refreshRate;    //in pictures/s
    float speed;        //px/s
    int spinTime;
    int speedUpTime;
    bool movDigWnEqual;

	bool clickable;

    SingleCoil *lowerCoil;
    SingleCoil *higherCoil;
	QPixmap *pCoilImg;
	QPixmap *sCoilImg;

    QTime *frameTime;           //counts the time the program neats
                                //to produce the last frame
    QTimer *loopBrake;          //shuts down the frameloop if frames are
                                //created faster than the scereen can show it
};

inline double DoubleCoil::bySC(double n)
{
    return n*sizeCoefficient;
}

inline double DoubleCoil::dbC(double n)
{
    return n/sizeCoefficient;
}

//getter/setter

inline bool DoubleCoil::isReady() const
{
    return (lowerCoil->isReady() &&
        higherCoil->isReady());
}

inline int DoubleCoil::getValue() const
{
    return containingNumber;
}

inline float DoubleCoil::getSpeed() const
{
    return speed;
}

inline int DoubleCoil::getSpinTime() const
{
    return spinTime;
}

inline int DoubleCoil::getSpeedUpTime() const
{
    return speedUpTime;
}

inline bool DoubleCoil::isMovDigWnEqual() const
{
    return movDigWnEqual;
}

inline void DoubleCoil::setSmallDigit(QString n)
{
    smallestDigit = n;
}

inline void DoubleCoil::setBigDigit(QString n)
{
    biggestDigit = n;
}

inline void DoubleCoil::setSpeed(float v)
{
    speed = v;
}

inline void DoubleCoil::setSpinTime(int v)
{
    spinTime = v;
}

inline void DoubleCoil::setSpeedUpTime(int v)
{
    speedUpTime = v;
}

inline void DoubleCoil::setMovDigWnEqual(bool v)
{
    movDigWnEqual = v;
}

//input

inline void DoubleCoil::setClickable(bool v)
{
    clickable = v;
}

#endif
