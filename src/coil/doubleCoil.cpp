 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * doubleCoil.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "doubleCoil.hpp"
#include "singleCoil.hpp"
#include <QtWidgets>
#include <iostream>
#include <climits>

extern QApplication *app;

DoubleCoil::DoubleCoil(QWidget *parent)
    : QWidget(parent)
	,containingNumber(0)
    ,smallestDigit("00")
    ,biggestDigit("99")
    ,refreshRate(60)
    ,speed(5000)
    ,spinTime(5)
    ,speedUpTime(1000)
    ,movDigWnEqual(true)
    ,clickable(false)
{	
	setMinimumSize(stdWidth/10, stdHeight/10);
    setMaximumSize(stdWidth, stdHeight);
	setSC();
		
    higherCoil = new SingleCoil();
    lowerCoil = new SingleCoil();
	pCoilImg = new QPixmap(bySC(stdSCoilWidth), bySC(stdSCoilHeight));
	sCoilImg = new QPixmap(bySC(stdSCoilWidth), bySC(stdSCoilHeight));

	frameTime = new QTime();
	loopBrake = new QTimer();
	
	connect(loopBrake, SIGNAL(timeout()),
		this, SLOT(main_loop()));
	connect(this, SIGNAL(main_loopRevind()),
		this, SLOT(main_loop()));
}

void DoubleCoil::paintEvent(QPaintEvent *)
{	
    higherCoil->render(sCoilImg);		//process single Coils
    lowerCoil->render(pCoilImg);
	
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setWindow(0, 0, stdWidth, stdHeight);
	painter.setViewport(0, 0, stdWidth, stdHeight);
	
                                        //draw single Coils
    painter.drawPixmap(bySC(lineSize-6), bySC(lineSize-6), *sCoilImg);
    painter.drawPixmap(bySC(200+lineSize-6), bySC(lineSize-6), *pCoilImg);
		
	QPen pen = QPen(Qt::black);			//black frame
	pen.setWidth(bySC(lineSize));
	painter.setPen(pen);	
	painter.drawRoundRect(bySC(lineSize/2), bySC(lineSize/2), 
				bySC(stdWidth-lineSize), bySC(stdHeight-lineSize));	
	
	pen.setColor(Qt::white);			//white frame
	pen.setWidth(bySC(lineSize/4));
	painter.setPen(pen);
	painter.drawRoundRect(bySC(lineSize/2), bySC(lineSize/2), 
				bySC(stdWidth-lineSize), bySC(stdHeight-lineSize));
}

void DoubleCoil::main_loop()
{
	if(!isReady())
	{
        int ft = frameTime->elapsed();
        if(ft < 1000/refreshRate)
		{
            loopBrake->start((1000/refreshRate)-ft);
			return;
		}
		frameTime->restart();
        lowerCoil->init_frame(ft);
        higherCoil->init_frame(ft);
		repaint();
		emit main_loopRevind();
	}
	else
	{
        loopBrake->stop();
			emit ready();
    }

}

void DoubleCoil::moveTo(int rn)
{
    if(speedUpTime > spinTime)
    {
        std::cerr<<"Warning: not garanted what happens when speedUpTime( "<<
                   speedUpTime<<" ) is bigger than spinTime ( "<<spinTime<<" )"<<std::endl;
    }

    //the next three objects are neaded to get each digit
    //of the numbers
    QString pNumber = QString::number(containingNumber);    //preveous number
                                                            //number from witch the coil
                                                            //shall start to move
    QString rNumber = QString::number(rn);
    frameTime->start();

    if(containingNumber < 10)
        pNumber.push_front("0");

    if(rn < 10)
        rNumber.push_front("0");



    higherCoil->init_moving(pNumber.at(0).toLatin1()-'0',
                               rNumber.at(0).toLatin1()-'0',
                               smallestDigit.at(0).toLatin1()-'0',
                               biggestDigit.at(0).toLatin1()-'0',
                               spinTime,
                               speed,
                               speedUpTime,
                               movDigWnEqual);
    lowerCoil->init_moving(pNumber.at(1).toLatin1()-'0',
                             rNumber.at(1).toLatin1()-'0',
                             smallestDigit.at(1).toLatin1()-'0',
                             biggestDigit.at(1).toLatin1()-'0',
                             spinTime,
                             speed,
                             speedUpTime,
                             movDigWnEqual);



    containingNumber = rn;
	main_loop();
}

void DoubleCoil::setTo(int r)
{
    QString rNumber = QString::number(r);
    if(r < 10)
        rNumber.push_front('0');
    higherCoil->init_setting(
                rNumber.at(0).toLatin1()-'0');
    lowerCoil->init_setting(
                rNumber.at(1).toLatin1()-'0');
    containingNumber = r;
    repaint();
}

void DoubleCoil::setSC()
{
	sizeCoefficient = width()/stdWidth > height()/stdHeight ?
				height()/stdHeight :
				width()/stdWidth;
}

void DoubleCoil::resizeEvent(QResizeEvent *)
{
	setSC();
    lowerCoil->setSizeCoff(bySC(1));
    higherCoil->setSizeCoff(bySC(1));
    delete pCoilImg;
	delete sCoilImg;
	pCoilImg = new QPixmap(bySC(stdSCoilWidth), bySC(stdSCoilHeight));
    sCoilImg = new QPixmap(bySC(stdSCoilWidth), bySC(stdSCoilHeight));
}

//input


void DoubleCoil::mousePressEvent(QMouseEvent *)
{
	if(clickable)
		emit clicked();
}
