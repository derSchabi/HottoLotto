 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * singleCoil.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SINGLECOIL_H_
#define SINGLECOIL_H_

#include <QWidget>
#include <QFontDatabase>
#include <string>

class QPushButton;
class QPaintEvent;
class QPixmap;

class SingleCoil
	: public QWidget
{
	Q_OBJECT
public:
	SingleCoil(QWidget *parent = 0);
    void init_moving(int, int, int, int, float, float, int, bool);	//must be callen exactly before
                                                        //start moving
                                                        //
                                                        //parameters:
                                //int initialNumber, int numberToReach, int minNumOnCoil,
                                //int maxNumOnCoil, int timeLengthAnimation, int speed //(px/s)
                                //int accelerationTime, bool movDigedWhenEqual
    void init_setting(int);     //just set a number without moving
    void init_frame(int&);
    void setSizeCoff(float);

	bool isReady() const;	
signals:
	void closed();
private slots:
	void close();
private:
    void loadStatic();          //runs only once. When the first object is created

	//height and width of widget
	//and drawing window
	static const int width = 200;
	static const int height = 300;

	//middle possition of the number	
	
    static const int xFontOffset = -5;
    static const int fontSize = 330;    //in pixel size
    static const int yFontOffset = -30;

	
	//start possition for the incoming number
	//depending to the offset
	static const int xEnterPos = 300;

	//end possition for the leaving number
	//depending to the offset
	static const int xLeavingPos = -300;	

    float speed;			//in pixels per second
                            //speed witch should be reached
                            //this is not used to create speedup and slow down
                            //animations. So this doesen`t contain the actual speed.
    float acSpeed;          //Actual speed witch the numbers realy have
                            //in px/s

    quint64 lastDrawTime;	//point of time since the lanst drawing
	quint64 enterTime;		//point of time of entering the animation
	int leavingNumber;		//number wich leaves the coil
	int enteringNumber;		//number wich enters the coil
	float enterNPos; 		//position of the entering number
                            //depending to the offset
	float leavingNPos; 		//position of the leaving number
                            //depending to the offset
	int reachNumber; 		//number that should be reached
                            //by the coil
    int minNumber;          //stores the lowest Number
                            //the coil can show
    int maxNumber;			//stores the heighest Number
                            //the coil can show
	float spinTime;			//contains how long the coil should spin
                            //in seconds
    float sizeCoefficient;  //for setting the viewport size
                            //that it maches into the framebuffer
                            //of the doubleCoil
    int accelerationTime;   //the time acSpeed should take to reach speed

    bool settingNumber;


	
	void paintEvent(QPaintEvent *);
	QPainter* init_painter();

	QTime *elapsedSpinTime;
    static QPixmap *shadow;

	friend class DoubleCoil;
};


inline void SingleCoil::setSizeCoff(float coff)
{
    sizeCoefficient = coff;
}

inline bool SingleCoil::isReady() const
{
    return !acSpeed;
}

inline void SingleCoil::loadStatic()
{
    static bool staticLoaded = false;

    if(staticLoaded == false)
    {
        shadow = new QPixmap(":images/shadow.png", "PNG");
        QFontDatabase::addApplicationFont("://DejaVuSans.ttf");
        staticLoaded = true;
    }
}


#endif
