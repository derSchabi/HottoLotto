 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * singleCoil.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtWidgets>
#include <iostream>
#include "singleCoil.hpp"

QPixmap *SingleCoil::shadow = 0;

SingleCoil::SingleCoil(QWidget *parent)
	: QWidget(parent)
	,acSpeed(0)
	,lastDrawTime(0)
	,leavingNumber(0)
	,enteringNumber(1)
	,enterNPos(xEnterPos)
    ,leavingNPos(0)
    ,accelerationTime(1000)
    ,settingNumber(false)
{
	setMinimumSize(width, height);
    elapsedSpinTime = new QTime();
    loadStatic();
}	

void SingleCoil::close()
{
	emit closed();
}

void SingleCoil::init_frame(int &tslf)
{
    //init frame

        if(acSpeed != 0)
        {
            //init acSpeed (for fliping effects)
            //and coil controle
            if((elapsedSpinTime->elapsed() >= (spinTime))	//slow down
                    && (enteringNumber == reachNumber))
            {
                acSpeed = (int)(speed*((-1*enterNPos)/(-1*xEnterPos)));
                if(enterNPos < 0)
                    acSpeed = 0;
            }
            else if(elapsedSpinTime->elapsed() < accelerationTime)	//speed up
                acSpeed = speed*
                    (elapsedSpinTime->elapsed()/(float)accelerationTime);
            else						//standart standart speed
                acSpeed = speed;

            leavingNPos -= (acSpeed*tslf/1000);
            enterNPos -= (acSpeed*tslf/1000);

            if(leavingNPos <= xLeavingPos)
            {
                leavingNPos = 0;
                enterNPos = xEnterPos;
                leavingNumber = enteringNumber;
                enteringNumber++;
                if(enteringNumber > maxNumber)
                    enteringNumber = minNumber;
            }
        }

}

QPainter* SingleCoil::init_painter()
{
    QPainter* painter = new QPainter(this);
    painter->setRenderHint(QPainter::Antialiasing, true);

    painter->setViewport(0, 0, width*sizeCoefficient, height*sizeCoefficient);

    painter->setWindow(0, 0, width, height);
    QFont font("DejaVu Sans");
    font.setPixelSize(fontSize);
    painter->setFont(font);
    return painter;
}

void SingleCoil::paintEvent(QPaintEvent *)
{	
	//init painter
	QPainter *painter = init_painter();
	painter->setBrush(QBrush(Qt::white, Qt::SolidPattern));
    painter->setPen(QPen(Qt::black, Qt::MiterJoin));

    //draw background
	painter->drawRect(0,0, width, height);
	//drawText
	painter->drawText(xFontOffset, height+yFontOffset+leavingNPos, 
				QString::number(leavingNumber));
	painter->drawText(xFontOffset, height+yFontOffset+enterNPos, 
				QString::number(enteringNumber));
	//draw shadow
    painter->drawPixmap(0, 0, *shadow);
    delete painter;
}

void SingleCoil::init_moving(int init, int r, int min, int max, float mt, float sp, int accTime, bool movIfEqual)
{
    if(!(!movIfEqual &&
            reachNumber == r))
    {
        leavingNumber = init;
        if(init+1 > max)
            enteringNumber = 0;
        else
            enteringNumber = init+1;

        leavingNPos = 0;
        enterNPos = xEnterPos;

        reachNumber = r;
        minNumber = min;
        maxNumber = max;
        acSpeed = speed = sp;
        spinTime = mt;
        accelerationTime = accTime;

        elapsedSpinTime->start();
    }

}

void SingleCoil::init_setting(int r)
{
    reachNumber = r;

    leavingNumber = reachNumber;
    enteringNumber = leavingNumber+1;
    if(enteringNumber > maxNumber)
        enteringNumber = minNumber;
    leavingNPos = 0;
    enterNPos = xEnterPos;
}





