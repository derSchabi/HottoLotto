 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * generator.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generator.hpp"
#include "random.hpp"
#include "settings/settings.hpp"
#include "global.hpp"
#include <QtCore>
#include <iostream>
#include <algorithm>

Generator::Generator(QObject *parent)
	: QObject(parent)
    ,generatorTyp(stdRandom)
    ,postRandom(false)
    ,careNum(false)
    ,careCom(false)
    ,mother(0)
    ,manuallyWidget(new Manually())
    ,useManually(false)
    ,usedBase(0)
    ,targetBase(0)
    ,baseFile(0)
{
    baseFile = getBaseFile();
}

void Generator::init(QWidget *m)		//initalize generator
{
	mother = m;
	
    generatorTyp = (GeneratorTyp)settings->getGenerator();
    postRandom = settings->isPostRandom();

    if(settings->getGenerator() == manually)
    {
        useManually = true;
        manuallyWidget->setUnset();
    }

    if(useManually && !postRandom)
        careCom = careNum = false;
    else {
        careCom = settings->isComOnce();
        careNum = settings->isNumOnce();
    }

    delete usedBase;
    if(careCom)
        usedBase = getBase(baseFile);
    else
        usedBase = new Base();
	
    generateAllCombi();

    //debug and info output
	for(int x = 1; x <= 3; x++)
	{
		for(int y = 1; y <= 3; y++)
			std::cout<<getRand(x, y)<<" ";
		std::cout<<std::endl;
	}
	std::cout<<std::endl;	
}

QFile* Generator::getBaseFile()
{
        QString home(QDir::homePath());

        #if defined _WIN32 || defined _WIN64
                QString dataPath("/appdata/Roaming/");
                QString dataFolder("HottoLotto");
        #endif

        #ifdef __linux
                QString dataPath("/");
                QString dataFolder(".HottoLotto");
        #endif

        #ifdef __APPLE__
                QString dataPath("/Library/Application Support/");
                QString dataFolder("HottoLotto");
        #endif
        
        QString fileName("HottoLottoSave.dat");
    
        QDir dir(home+dataPath+dataFolder);
        QFile *baseFile = new QFile(dir.absolutePath()+"/"+fileName);
	
                
        if(!baseFile->exists()){
                QDir(home+dataPath).mkdir(dataFolder);
                baseFile->open(QIODevice::WriteOnly);
                baseFile->close();
                return baseFile;
        }
                return baseFile;
}

Base* Generator::getBase(QFile *file)
{
	Base *base = new Base;
	if(file->size() && !(file->size()%3))
	{
		int numberOfCombi = file->size()/3;
		
		file->open(QIODevice::ReadOnly);
		for(int i = 0; i < numberOfCombi; i++)
		{
			base->push_back(new char[3]);
			file->read(base->last(), 3);	
		}	

		file->close();
	}else if(file->size()%3)
	{
		std::cerr<<"HottoLottoBase.dat File broaken"<<std::endl;
		emit fileBroakenError();
	}
	return base;
}

bool Generator::isComAvailable(char *combi, Base* base)		//test if the given combinations are still available
                                                            //in the given base
{	
    QVectorIterator<char*> i(*base);
	char *puffer;
	while(i.hasNext())
	{
		puffer = i.next();
		if(puffer[0] == combi[0] &&
			puffer[1] == combi[1] &&
			puffer[2] == combi[2])
			return false;
        if(puffer[0] == combi[0] &&
            puffer[1] == combi[2] &&
            puffer[2] == combi[1])
            return false;
        if(puffer[0] == combi[1] &&
            puffer[1] == combi[0] &&
            puffer[2] == combi[2])
            return false;
        if(puffer[0] == combi[1] &&
            puffer[1] == combi[2] &&
            puffer[2] == combi[0])
            return false;
        if(puffer[0] == combi[2] &&
            puffer[1] == combi[0] &&
            puffer[2] == combi[1])
            return false;
        if(puffer[0] == combi[2] &&
            puffer[1] == combi[1] &&
            puffer[2] == combi[0])
            return false;

	}
	return true;	
}

bool Generator::isNumAvailable(bool *available, int n)
{
    if(careNum)
        return available[n-1];
    else
        return true;
}

void Generator::setNumUnavailable(bool *a, char *combi)
{
	for(int i = 0; i < 3; i++)
		a[((int)combi[i])-1] = false;
}

void Generator::generateAllCombi()         //generate all combinations
{
    bool *numAvail = 0;                     //array witch contains the still
                                            //available numbers
	if(careNum)
	{
		numAvail = new bool[25];
		std::fill_n(numAvail, 25, true);
    }

    targetBase = new Base;

	char *combi;
    if(!(useManually && !postRandom))
    {
        for(int i = 0; i < 3; i++)	//generate 3 combinations
        {

            do{
                combi = generateSingleCombi(numAvail);
            } while(!isComAvailable(combi, usedBase));

            if(careNum)
                setNumUnavailable(numAvail, combi);

            targetBase->push_back(combi);
            usedBase->push_back(combi);
        }
    }else {
        for(int i = 0; i < 3; i++)
            targetBase->push_back(generateSingleCombi(numAvail));
    }
    if(careCom)
        writeTargetBToFile(baseFile);

    delete numAvail;
}

char* Generator::generateSingleCombi(bool *numAvail)        //generate a single combination
{
	char *combi = new char[3];
    if(!(useManually && !postRandom))
    {
        do{
            combi[0] = getRandom();
        }while(!isNumAvailable(numAvail,combi[0]));

        do{
            combi[1] = getRandom();
        }while(combi[1] == combi[0] ||
            !isNumAvailable(numAvail,combi[1]));

        do{
            combi[2] = getRandom();
        }while(combi[2] == combi[1] ||
            combi[2] == combi[0] ||
            !isNumAvailable(numAvail, combi[2]));
    } else {
        for(int i = 0; i < 3; i++)
            combi[i] = getRandom();
    }
	return combi;
}

void Generator::writeTargetBToFile(QFile *file)
{
	file->open(QIODevice::Append);	
	for(int i = 0; i < 3; i++)	
        file->write(targetBase->at(i), 3);

	file->close();
}

void Generator::resetFile()
{
    QFile *file = getBaseFile();

	file->open(QIODevice::WriteOnly);	
	file->close();
	delete file;
}

int Generator::getRandom()
{
    int raw;
    switch(generatorTyp)
    {
    case stdRandom:
        raw = getStdRandom();
        break;
    case qRandom:
        raw = getQRandom(mother);

        break;
    case timedRandom:
        raw = getTimedRandom();
        break;
    case manually:
        raw = manuallyWidget->getNumber();
        break;
    default:
        std::cerr<<"Criticle Error: Generator Typ "<<generatorTyp<<" not known."<<std::endl;
        return 0;
    }

    srand(raw);
    int target = rand()%25+1;
    std::cout<<target<<std::endl;
    if(postRandom)
        return target;
    else
        return raw;

}
