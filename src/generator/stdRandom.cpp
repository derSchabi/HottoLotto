 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * random.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "random.hpp"
#include <iostream>

//nur für Mac und Linux
#if defined __APPLE__ || defined __linux__

#include <fstream>
#include <cstdlib>

int getStdRandom()
{
        char aPuffer[sizeof(int)];
	unsigned int *a = (unsigned int*)aPuffer;

        std::ifstream file;
        file.open("/dev/urandom");
        file.read(aPuffer, sizeof(int));
        file.close();

	srand(*a);	
	return (rand() % 25) + 1;
}

#endif

//nur für Windows
#if defined _WIN32 || defined _WIN64

#include <windows.h>
#include <wincrypt.h>
#pragma comment(lib, "advapi32.lib")

int getStdRandom()
{
    HCRYPTPROV hProvider = 0;

    if (!::CryptAcquireContextW(&hProvider, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_SILENT))
        std::cout<<"Random init fail"<<std::endl;

    const DWORD dwLength = 1;
    BYTE pbBuffer[dwLength] = {};

    if (!::CryptGenRandom(hProvider, dwLength, pbBuffer))
    {
        ::CryptReleaseContext(hProvider, 0);
        std::cout<<"Random Fail"<<std::endl;
    }

    if (!::CryptReleaseContext(hProvider, 0))
        std::cout<<"Random Release Fail"<<std::endl;

    return (static_cast<unsigned int>(pbBuffer[0])%25)+1;
}

#endif

//testet random funktion wenn program mit HottoLotto --test aufgerufen wurde

int testStdRandom()
{
	unsigned int nTry = 0;
	unsigned int counter[25] = {0};
	
    while(nTry < 10000)
	{
		//versuch
		nTry++;
		counter[getStdRandom()-1]++;

		//ausgabe
		std::cout<<"As cloaser the numbers are to one as better the Algorithm is working!!!"<<std::endl;
		std::cout<<"Try : "<<nTry<<std::endl;

		for(int i = 0; i < 25; i++)
			std::cout<<i+1<<" : "<<((double)counter[i]/(double)nTry)*25<<std::endl;

		std::cout<<std::endl;	
	}
	return 0;
}
