 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * manually.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "manually.h"
#include "ui_manually.h"
#include "generator/random.hpp"
#include <iostream>

Manually::Manually(QWidget *parent)
    :QDialog(parent)
    ,ui(new Ui::Manually)
    ,numbersAreSet(false)
    ,randomTry(0)
{
    ui->setupUi(this);
}

Manually::~Manually()
{
    delete ui;
}

int Manually::getNumber()
{
    if(!numbersAreSet)
    {
        this->exec();
        numbersAreSet = true;
    }
    randomTry++;

    switch(randomTry)
    {
        case 1:
            return ui->r1t1Box->value();
        case 2:
            return ui->r1t2Box->value();
        case 3:
            return ui->r1t3Box->value();
        case 4:
            return ui->r2t1Box->value();
        case 5:
            return ui->r2t2Box->value();
        case 6:
            return ui->r2t3Box->value();
        case 7:
            return ui->r3t1Box->value();
        case 8:
            return ui->r3t2Box->value();
        case 9:
            return ui->r3t3Box->value();
        default:
            std::cerr<<"Manualy Error: more values taken than given\n "
                       "using getStdRandom() for now"<<std::endl;
            return getStdRandom();
    }

}

void Manually::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::LanguageChange)
        ui->retranslateUi(this);
    else
        QDialog::changeEvent(e);
}
