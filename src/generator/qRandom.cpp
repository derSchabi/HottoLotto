 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * qRandom.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtNetwork>
#include <QObject>
#include <iostream>
#include <cstdlib>
#include <QMessageBox>

#include "generator.hpp"
#include "random.hpp"

QStringList processReply(QNetworkReply *, QMessageBox &);

int getQRandom(QWidget *mother = 0)
{
	static int numInQuee = 1;
    static QStringList quee;

	if(numInQuee == 1)	//if nomore numbers are available aquire new one
	{
		QMessageBox info(
            QMessageBox::NoIcon,
            QWidget::tr("Connecting www.randomnumbers.info"),
            QWidget::tr("Connecting to www.randomnumbers.info"),
			QMessageBox::NoButton,
			mother);
			
		info.show();
		//process connection
		QNetworkAccessManager net;
		QEventLoop loop;
		QObject::connect(&net, SIGNAL(finished(QNetworkReply*)),
			&loop, SLOT(quit()));
		

		//sending request
        info.setText(QWidget::tr("Sending Request"));
		QNetworkRequest request(QUrl(
			"http://www.randomnumbers.info/cgibin/wqrng.cgi"));
		request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
		QByteArray postData;
		postData.append("amount=90&limit=24&.submit=Daten+absenden");
		
        info.setText(QWidget::tr("Waiting for Reply"));
		QNetworkReply *reply = net.post(request, postData);
		loop.exec();
		quee = processReply(reply, info);
		if(quee.length() != 0)
		{
			numInQuee = 90;
			info.close();
		} else {
			info.exec();
			return 0;	
		}
	}

	return quee.at(--numInQuee).toInt()+1;
	
}

QStringList processReply(QNetworkReply *reply, QMessageBox &info)
{
    QStringList valueList;

	if(!reply->error() == QNetworkReply::NoError)
	{
        QString errorMessage = QWidget::tr("Couldn`t reach server");
        QString infoMessage = QWidget::tr("Using /dev/urandom instead");
#if defined _WIN32 || defined _WIN64
        infoMessage = QWidget::tr("Using Windows crypt API instead");
#endif
        std::cerr<<errorMessage.toStdString()<<std::endl;
		info.setText(errorMessage);
		info.exec();
		info.setText(infoMessage);
		info.exec();
		for(int i = 0; i < 90; i++)
            valueList.push_back(QString::number(getStdRandom()));
	} else {
		QByteArray puffer;
        QString value;

		puffer = reply->readAll();
        value = QString(puffer.data());
		value.remove(0, 0x71d);
		value.remove(QRegExp("[^0-9 ]"));
		valueList = value.split(" ");

		//for(int i = 0; i < 90; i++)
			//std::cout<<valueList.at(i).toInt()+1<<std::endl;
	} 
	return valueList;
}
