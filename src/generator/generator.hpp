 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * generator.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATOR_H_
#define GENERATOR_H_

#include <QObject>
#include <QFile>
#include <QVector>
#include "manually/manually.h"

typedef QVector<char*> Base;	//saving constelations of numbers

class Generator
	: public QObject
{
	Q_OBJECT
public:
    enum GeneratorTyp{
        stdRandom,
        qRandom,
        timedRandom,
        manually
    };

	Generator(QObject *parent = 0);

    void init(QWidget *m = 0);      //QWidget motherWidget

    int getRand(int, int);      //int colum, int row

public slots:
	void resetFile();
signals:
	void fileBroakenError();
private:
	int getRandom();
    QFile* getBaseFile();
	Base* getBase(QFile*);
    bool isComAvailable(char*, Base*);

	bool isNumAvailable(bool*, int);
    void setNumUnavailable(bool*, char*);

    void generateAllCombi();
	char* generateSingleCombi(bool*);

    void writeTargetBToFile(QFile*);

    GeneratorTyp generatorTyp;
    bool postRandom;

    bool careNum;           //if true numbers appear only once per game
    bool careCom;           //if true combinations appear only once ever

	QWidget *mother;

    Manually *manuallyWidget;
    bool useManually;

    Base *usedBase;			//base witch contains the combinations for the later use
    Base *targetBase;           //base witch contains the combinations witch
                                //where allready in use
    QFile *baseFile;
};


inline int Generator::getRand(int lap, int coil)
{
    return targetBase->at(lap-1)[coil-1];
}

#endif
