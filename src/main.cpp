 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * main.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QPushButton>
#include <QSettings>
#include <string>
#include <iostream>

#include "generator/random.hpp"
#include "settings/settings.hpp"
#include "global.hpp"
#include "core.hpp"

QApplication *app;
Settings *settings;

int trol()
{
    std::cout<<"Trollolllolllol"<<std::endl;
    return 0;
}

int main(int argc, char **argv)
{
	if(argc > 1 && (!strcmp(argv[1], "--test") ||
			!strcmp(argv[1], "-t")))
		return testStdRandom();
    if(argc > 1 && (!strcmp(argv[1], "--cheat") ||
                    !strcmp(argv[1], "-c")))
        return trol();

    Core *core = 0;

    int retNum;             //QApplication return code
	


    app = new QApplication(argc, argv);
    settings = new Settings();

    core = new Core();
    core->show();
    retNum = app->exec();
    std::cout<<"QApplication return code: "<<retNum<<std::endl;

    return retNum;
}	
