 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * core.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtWidgets>
#include <iostream>
#include "core.hpp"
#include "lottoWindow.hpp"
#include "controlWindow.h"
#include "ui_controlWindow.h"
#include "settings/settings.hpp"
#include "settings/settingsDialog.hpp"
#include "generator/generator.hpp"
#include "global.hpp"

Core::Core(QObject *parent)
    : QObject(parent)
    ,lottoWindow(new LottoWindow())
    ,controlWindow(new ControlWindow())
    ,settingsDialog(new SettingsDialog(controlWindow))
    ,generator(new Generator(this))
    ,round(0)
    ,randomTry(0)
{
    loadLanguage();
    generator->init(controlWindow);

    fullscreen = settings->isFullscreen();

    connect(controlWindow, SIGNAL(startButtonClicked()),
            this, SLOT(onStart()));
    connect(controlWindow, SIGNAL(coilButtonClicked()),
            this, SLOT(coilClicked()));
    connect(controlWindow, SIGNAL(nextButtonClicked()),
            this, SLOT(nextRound()));
    connect(controlWindow, SIGNAL(fullscreenClicked()),
            this, SLOT(onFullscreenChange()));
    connect(controlWindow, SIGNAL(settingsClicked()),
            this, SLOT(onSettings()));
    connect(controlWindow, SIGNAL(resetClicked()),
            this, SLOT(onReset()));

    connect(settingsDialog, SIGNAL(resetFile()),
            generator, SLOT(resetFile()));
    connect(settingsDialog, SIGNAL(okClicked()),
            this, SLOT(onReset()));

    connect(lottoWindow, SIGNAL(ready()),
            this, SLOT(onReady()));
    connect(lottoWindow, SIGNAL(coilClicked()),
            this, SLOT(coilClicked()));
    connect(lottoWindow->miniButton, SIGNAL(clicked()),
            this, SLOT(onFullscreenChange()));
    connect(lottoWindow->revindButton, SIGNAL(clicked()),
            this, SLOT(onReset()));
    connect(lottoWindow, SIGNAL(actionKeyPressed()),
            this, SLOT(coilClicked()));
    connect(lottoWindow, SIGNAL(escapePressed()),
            this, SLOT(onFullscreenChange()));
}

void Core::show()
{
    lottoWindow->miniButton->setFullscreen(fullscreen);
    controlWindow->ui->actionFullscreen->setChecked(fullscreen);
    if(fullscreen)
    {
        controlWindow->show();
        lottoWindow->show();
        lottoWindow->setWindowState(Qt::WindowFullScreen);
    } else {
        lottoWindow->show();
        controlWindow->show();
    }
    //after Effect
    lottoWindow->resetCoils();
}

void Core::coilClicked()
{
    if(round == 3 && randomTry == 4)
        return;
    if(lottoWindow->isReady())
    {
        for(int i = 0; i < 3; i++)
            lottoWindow->coil[i]->setClickable(false);
        if(!round)
            onStart();
        else if(randomTry == newRoundPending)
            nextRound();
        else
            nextTry();
    }
}


void Core::onStart()
{
     controlWindow->ui->cb1->show();
     controlWindow->ui->cb2->show();
     controlWindow->ui->cb3->show();

     controlWindow->ui->cb2->setEnabled(false);
     controlWindow->ui->cb3->setEnabled(false);

     lottoWindow->coil[0]->setClickable(true);

     controlWindow->ui->startButton->hide();
     randomTry = newRoundPending;

     round++;
     randomTry = 0;
     addMessageToList(round, QString(
                 tr("Round") + " " + QString::number(round) + ":"));
}

void Core::nextRound()
{
    controlWindow->ui->cb1->setText("00");
    controlWindow->ui->cb2->setText("00");
    controlWindow->ui->cb3->setText("00");

    controlWindow->ui->cb1->show();
    controlWindow->ui->cb2->show();
    controlWindow->ui->cb3->show();

    controlWindow->ui->nextButton->hide();

    if(round != 0)
        lottoWindow->resetCoils();
    round++;
    randomTry = 0;

    addMessageToList(round, QString(
                tr("Round") + " " + QString::number(round) + ":"));
}

void Core::nextTry()
{
    randomTry++;

    controlWindow->ui->cb1->setEnabled(false);
    controlWindow->ui->cb2->setEnabled(false);
    controlWindow->ui->cb3->setEnabled(false);

    for(int i = 0; i < 3; i++)
        lottoWindow->coil[i]->setClickable(false);


    int randomNum = generator->getRand(round, randomTry);
    lottoWindow->coil[randomTry-1]->moveTo(randomNum);
}

void Core::onReady()
{
    if(!lottoWindow->wasResetingCoils())
    {
        QString outcom = QString::number(lottoWindow->coil[randomTry-1]->getValue());

        addMessageToList(round, outcom);
        for(int i = 0; i < 3; i++)
            lottoWindow->coil[i]->setClickable(false);

        switch(randomTry)
        {
        case 1:
            controlWindow->ui->cb1->setText(outcom);
            controlWindow->ui->cb2->setEnabled(true);
            lottoWindow->coil[1]->setClickable(true);
            break;

        case 2:
            controlWindow->ui->cb2->setText(outcom);
            controlWindow->ui->cb3->setEnabled(true);
            lottoWindow->coil[2]->setClickable(true);
            break;

        case 3:
            for(int i = 0; i < 3; i++)
                lottoWindow->coil[i]->setClickable(true);

            controlWindow->ui->cb3->setText(outcom);
            randomTry = newRoundPending;

            controlWindow->ui->cb1->hide();
            controlWindow->ui->cb2->hide();
            controlWindow->ui->cb3->hide();
            if(round != 3)
                controlWindow->ui->nextButton->show();
            else {
                for(int i = 0; i < 3; i++)
                    lottoWindow->coil[i]->setClickable(false);
                controlWindow->ui->quitButton->show();
                controlWindow->ui->resetButton->show();
                lottoWindow->revindButton->show();
            }

            break;

        default:
            std::cerr<<"Criticle Error: randomTry = "<<randomTry<<std::endl;
        }
    } else {
        controlWindow->ui->cb1->setEnabled(true);
        lottoWindow->coil[0]->setClickable(true);
    }

}

void Core::addMessageToList(int l, QString txt)
{
    switch(l)
    {
    case 1:
        controlWindow->ui->list1->append(txt);
        break;
    case 2:
        controlWindow->ui->list2->append(txt);
        break;
    case 3:
        controlWindow->ui->list3->append(txt);
        break;
    default:
        std::cerr<<"Error: list "<<l<<" does not exist"<<std::endl;
    }
    lottoWindow->table[l-1]->addText(txt);
}

void Core::onFullscreenChange()
{
    fullscreen = !fullscreen;
    lottoWindow->miniButton->setFullscreen(fullscreen);
    controlWindow->ui->actionFullscreen->setChecked(fullscreen);
    if(fullscreen)
        lottoWindow->setWindowState(Qt::WindowFullScreen);
    else {
        lottoWindow->setWindowState(Qt::WindowNoState);
        app->setActiveWindow(controlWindow);
        controlWindow->raise();
    }
}

void Core::onSettings()
{
    settingsDialog->show();
}

void Core::loadLanguage()
{
    QTranslator *translator = new QTranslator();
    QString lang = settings->getLang();
    QString langFile = QString("hottolotto_") + lang;

    translator->load(QString("hottolotto_") + lang, ":translib");
    app->installTranslator(translator);
}

void Core::onReset()
{
    loadLanguage();
    randomTry = 0;
    round = 0;
    generator->init(controlWindow);
    for(int i = 0; i < 3; i++)
    {
        lottoWindow->table[i]->reset();
    }

    lottoWindow->setSSS(settings->getSpeed(),
                        settings->getSpeedUpTime(),
                        settings->getSpinTime());
    lottoWindow->resetCoils();

    lottoWindow->revindButton->hide();

    controlWindow->ui->quitButton->hide();
    controlWindow->ui->resetButton->hide();
    controlWindow->ui->startButton->show();

    controlWindow->ui->list1->setText("");
    controlWindow->ui->list2->setText("");
    controlWindow->ui->list3->setText("");

    controlWindow->ui->cb1->setText("00");
    controlWindow->ui->cb2->setText("00");
    controlWindow->ui->cb3->setText("00");

    controlWindow->ui->cb1->hide();
    controlWindow->ui->cb2->hide();
    controlWindow->ui->cb3->hide();

    controlWindow->ui->nextButton->hide();
}
