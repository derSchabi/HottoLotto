 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * controlWindow.h
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTROLWINDOW_H
#define CONTROLWINDOW_H

#include <QDialog>
#include "settings/settingsDialog.hpp"

namespace Ui {
class ControlWindow;
}

class ControlWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit ControlWindow(QWidget *parent = 0);
    ~ControlWindow();
signals:
    void settingsClicked();
    void infoClicked();
    void quitClicked();
    void fullscreenClicked();
    void coilButtonClicked();
    void nextButtonClicked();
    void startButtonClicked();
    void resetClicked();
private slots:
    void onSettings();
    void onInfo();
    void onQuit();
    void onReset();
    void onFullscreen();
    void onCoilButton();
    void onNextButton();
    void onStartButton();
private:
    void closeEvent(QCloseEvent *);
    void changeEvent(QEvent *);

    Ui::ControlWindow *ui;

    friend class Core;
};

#endif // CONTROLWINDOW_H
