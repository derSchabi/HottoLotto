 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * closeWidget.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "closeWidget.hpp"
#include <QtWidgets>


CloseWidget::CloseWidget(QWidget *parent)
    : QWidget(parent)
{
	setMinimumSize(20, 20);
	setMaximumSize(20, 20);
}

void CloseWidget::mousePressEvent(QMouseEvent *)
{
	emit clicked();
}

void CloseWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setWindow(0, 0, width(), height());
	painter.setViewport(0, 0, width(), height());
		//draw background
	painter.setPen(QPen(Qt::black));	
	painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
	painter.drawRect(0, 0, width(), height());
		//draw cross
	painter.setPen(QPen(Qt::white, 5));
	painter.drawLine(0,0, 20, 20);
	painter.drawLine(0, 20, 20, 0);
}
