 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * core.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_H_
#define CORE_H_

#include <QObject>
#include "lottoWindow.hpp"
#include "controlWindow.h"
#include "settings/settings.hpp"
#include "generator/generator.hpp"

class Core :
	public QObject
{
	Q_OBJECT
public:
    Core(QObject *mother = 0);
    void show();
private slots:
    void onStart();
    void nextRound();
    void nextTry();
    void onReady();
    void coilClicked();
    void onFullscreenChange();
    void onSettings();
    void onReset();
private:
    void addMessageToList(int, QString);
    void loadLanguage();

    enum{
        newRoundPending = 4
    };

    LottoWindow *lottoWindow;       //friends with core
    ControlWindow *controlWindow;   //friends with core
                                    //so you can user private objects
    SettingsDialog *settingsDialog;

    Generator *generator;
    
    bool fullscreen;
    
    int round;      //holds the current round
    int randomTry;  //holds thr current try
};

#endif
