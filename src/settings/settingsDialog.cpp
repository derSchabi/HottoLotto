 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * settingsDialog.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settingsDialog.hpp"
#include "ui_settingsdialog.h"
#include "generator/generator.hpp"
#include <cstdlib>
#include "global.hpp"
#include "settings.hpp"
#include <iostream>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    infoDialog(new QErrorMessage),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);

#if defined _WIN32 || defined _WIN64
    ui->stdRandomRadio->setText(tr("Windows crypt API"));
    ui->stdRandomRadio->setToolTip(tr("using Windows crypt API to generate random numbers"));
#endif

    initLanBox();
    loadSettings();
    onManuallyChange();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::onResetButton()
{
    ui->resetButton->setEnabled(false);
	emit resetFile();
}

void SettingsDialog::onOkButton()
{
    settings->setFullscreen(ui->startFullscreenChk->isChecked());
    settings->setComOnce(ui->comOnceEverChk->isChecked());
    settings->setNumOnce(ui->numOnceChk->isChecked());

    settings->setLang(ui->lanBox->currentText());

    settings->setSpeed(ui->speedBox->value());
    settings->setSpinTime(ui->spinTimeBox->value());
    settings->setSpeedUpTime(ui->speedUpTimeBox->value());

    if(ui->stdRandomRadio->isChecked())
        settings->setGenerator(Generator::stdRandom);
    else if(ui->qRandomRadio->isChecked())
        settings->setGenerator(Generator::qRandom);
    else if(ui->tRandomRadio->isChecked())
        settings->setGenerator(Generator::timedRandom);
    else if(ui->manuallyRadio->isChecked())
        settings->setGenerator(Generator::manually);
    else
        std::cerr<<"Error: no fitting RandomRadio Button"<<std::endl;

    settings->setPostRandom(ui->postRandomBox->isChecked());

    close();
    emit okClicked();
}

void SettingsDialog::onCancel()
{
    loadSettings();
    close();
}

inline void SettingsDialog::initLanBox()
{
    ui->lanBox->addItem("en");
    ui->lanBox->addItem("de");
}

void SettingsDialog::loadSettings()
{
	//use settings declarated in global.hpp
    ui->startFullscreenChk->setChecked(settings->isFullscreen());
    ui->numOnceChk->setChecked(settings->isNumOnce());
    ui->comOnceEverChk->setChecked(settings->isComOnce());

    ui->lanBox->setCurrentIndex(
            ui->lanBox->findText(settings->getLang()));

    ui->speedBox->setValue(settings->getSpeed());
    ui->spinTimeBox->setValue(settings->getSpinTime());
    ui->speedUpTimeBox->setValue(settings->getSpeedUpTime());

    switch(settings->getGenerator())
    {
    case Generator::stdRandom:
        ui->stdRandomRadio->setChecked(true);
        break;
    case Generator::qRandom:
        ui->qRandomRadio->setChecked(true);
        break;
    case Generator::timedRandom:
        ui->tRandomRadio->setChecked(true);
        break;
    case Generator::manually:
        ui->manuallyRadio->setChecked(true);
        break;
    default:
        std::cerr<<"Criticle Error: Generator "<<settings->getGenerator()<<" not known."<<std::endl;
        std::cerr<<"Using stdRandom instead"<<std::endl;
        ui->stdRandomRadio->setChecked(true);
        settings->setGenerator(Generator::stdRandom);
    }

    ui->postRandomBox->setChecked(settings->isPostRandom());
}

void SettingsDialog::onManuallyChange()
{
    if(!ui->postRandomBox->isChecked() &&
            ui->manuallyRadio->isChecked())
    {
        ui->numOnceChk->setDisabled(true);
        ui->comOnceEverChk->setDisabled(true);
    } else {
        ui->numOnceChk->setEnabled(true);
        ui->comOnceEverChk->setEnabled(true);
    }
}

void SettingsDialog::loadingDefaults()
{
    ui->startFullscreenChk->setChecked(false);
    ui->numOnceChk->setChecked(false);
    ui->comOnceEverChk->setChecked(false);

    ui->speedBox->setValue(5000);
    ui->spinTimeBox->setValue(4000);
    ui->speedUpTimeBox->setValue(1000);

    ui->stdRandomRadio->setChecked(true);


    ui->postRandomBox->setChecked(false);
}

void SettingsDialog::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
#if defined _WIN32 || defined _WIN64
    ui->stdRandomRadio->setText(tr("Windows crypt API"));
    ui->stdRandomRadio->setToolTip(tr("using Windows crypt API to generate random numbers"));
#endif
    }
    else
        QDialog::changeEvent(e);
}
