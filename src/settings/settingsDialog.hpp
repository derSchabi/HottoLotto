 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * settingsDialog.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGSDIALOG_HPP
#define SETTINGSDIALOG_HPP

#include <QDialog>
#include <QErrorMessage>

class QComboBox;
class QCheckBox;
class QPushButton;
class Settings;

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

signals:
	void resetFile();
    void okClicked();
private slots:
    void onResetButton();
    void onOkButton();
    void onCancel();
    void onManuallyChange();        //triggered by all radio Buttons
                                    //and postRandomBox
    void loadingDefaults();
private:
    void initLanBox();
    void loadSettings();

    void changeEvent(QEvent *);

    QErrorMessage *infoDialog;
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_HPP
