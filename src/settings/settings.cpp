 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * settings.cpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.hpp"
#include "generator/generator.hpp"
#include <QtCore>
#include <iostream>


const QString Settings::startFullscreened = "startFullscreened";
const QString Settings::numOnlyOnce = "numOnlyOnce";
const QString Settings::comOnceEver = "comOnceEver";
const QString Settings::language = "language";
const QString Settings::speed = "speed";
const QString Settings::spinTime = "spinTime";
const QString Settings::speedUpTime = "speedUpTime";
const QString Settings::generator = "generator";
const QString Settings::postRandom = "postRandom";

Settings::Settings(QObject *parent)
    :QObject(parent)
{
    settings = new QSettings("Schabi", "HottoLotto");
    loadSettings();
}

void Settings::loadSettings()
{
     //if settings aren´t available create them
    if(settings->value(startFullscreened).isNull())
       settings->setValue(startFullscreened, false);
    if(settings->value(numOnlyOnce).isNull())
        settings->setValue(numOnlyOnce, false);
    if(settings->value(comOnceEver).isNull())
        settings->setValue(comOnceEver, false);
    if(settings->value(language).isNull())
        settings->setValue(language, "en");
    if(settings->value(speed).isNull())
        settings->setValue(speed, 5000);
    if(settings->value(spinTime).isNull())
        settings->setValue(spinTime, 4000);
    if(settings->value(speedUpTime).isNull())
        settings->setValue(speedUpTime, 1000);
    if(settings->value(generator).isNull())
        settings->setValue(generator, Generator::stdRandom);
    if(settings->value(postRandom).isNull())
        settings->setValue(postRandom, false);
}

