 /* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-" */
 /*
 * settings.hpp
 * Copyright (C) Christian Schabesberger 2013 <chris.schabesberger@gmail.com>
 * 
 * HottoLotto is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * HottoLotto is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>
#include <QSettings>
#include <iostream>

class QSettings;

class Settings
	: public QObject
{
public:
	Settings(QObject *parent = 0);
	
	bool isFullscreen() const;
	bool isNumOnce() const;
	bool isComOnce() const;
	QString getLang() const;
    int getSpeed() const;
    int getSpinTime() const;
    int getSpeedUpTime() const;
    int getGenerator() const;
    bool isPostRandom() const;
public slots:
	void setFullscreen(bool);
	void setNumOnce(bool);
	void setComOnce(bool);
	void setLang(QString);
    void setSpeed(int);
    void setSpinTime(int);
    void setSpeedUpTime(int);
    void setGenerator(int);
    void setPostRandom(bool);
private:
    void loadSettings();

    static const QString startFullscreened;
    static const QString numOnlyOnce;
    static const QString comOnceEver;
    static const QString language;
    static const QString speed;
    static const QString spinTime;
    static const QString speedUpTime;
    static const QString generator;
    static const QString postRandom;

	QSettings *settings;
};

inline bool Settings::isFullscreen() const
{
    return settings->value(startFullscreened).toBool();
}

inline bool Settings::isNumOnce() const
{
    return settings->value(numOnlyOnce).toBool();
}

inline bool Settings::isComOnce() const
{
    return settings->value(comOnceEver).toBool();
}

inline QString Settings::getLang() const
{
    return settings->value(language).toString();
}

inline int Settings::getSpeed() const
{
    return settings->value(speed).toInt();
}

inline int Settings::getSpinTime() const
{
    return settings->value(spinTime).toInt();
}

inline int Settings::getSpeedUpTime() const
{
    return settings->value(speedUpTime).toInt();
    std::cout<<settings->value(speedUpTime).toInt()<<std::endl;
}

inline int Settings::getGenerator() const
{
    return settings->value(generator).toInt();
}

inline bool Settings::isPostRandom() const
{
    return settings->value(postRandom).toBool();
}

inline void Settings::setFullscreen(bool v)
{
    settings->setValue(startFullscreened, v);
}

inline void Settings::setNumOnce(bool v)
{
    settings->setValue(numOnlyOnce, v);
}

inline void Settings::setComOnce(bool v)
{
    settings->setValue(comOnceEver, v);
}

inline void Settings::setLang(QString v)
{
    settings->setValue(language, v);
}

inline void Settings::setSpeed(int v)
{
    settings->setValue(speed, v);
}

inline void Settings::setSpinTime(int v)
{
    settings->setValue(spinTime, v);
}

inline void Settings::setSpeedUpTime(int v)
{
    settings->setValue(speedUpTime, v);
}

inline void Settings::setGenerator(int v)
{
    settings->setValue(generator, v);
}

inline void Settings::setPostRandom(bool v)
{
    settings->setValue(postRandom, v);
}


#endif
