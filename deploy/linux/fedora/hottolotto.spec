Name:		HottoLotto
Version:	0.9	
Release:	1%{?dist}
Summary:	A program to generate random numbers

Group:		Amusements/Games
License:	GPLv3
URL:		https://github.com/theScrabi
Source0:	%{name}-%{version}.tar.gz

BuildRequires:	gcc-c++,qt5-qtbase-devel
Requires:	qt5-qtbase

%description
HottoLotto is an easy to use application designed to help user
generate random numbers.

HottoLotto can be used to display various number for bingo and lotto games.

%prep
%setup -q -n %{name}

%build
./INSTALL --automake qmake-qt5


%install
./INSTALL --autoinstall %{buildroot}/usr %{buildroot}%{_datadir}/applications %{_bindir} %{_datadir}/pixmaps --install_doc


%files
%defattr(-,root,root,-)
%{_bindir}/hottolotto
%{_datadir}/pixmaps/hottolotto.png
%{_datadir}/applications/HottoLotto.desktop
%{_defaultdocdir}/hottolotto/copyright
%{_defaultdocdir}/hottolotto/README.md

%changelog
* Thu Oct 03 2013 Schabi <chris.schabesberger@gmail.com>
- First Build
