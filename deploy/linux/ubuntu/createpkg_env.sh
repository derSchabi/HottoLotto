#!/bin/bash

VER=0.9

INPUT=0

echo "This script will create a debian packaging friendly environment."
echo "!!!Do not copy and run this file from another directory as this!!!"
echo 
read -n 1 -p "Continue [y/n]:" INPUT
echo
if [ $INPUT != y ]
then
	exit 0
fi

echo "**********OUTPUT**********"
mkdir -v ../../../../hottolotto-$VER
echo "copying files from HottoLotto to hottolotto-$VER"
cp -r ../../../* ../../../../hottolotto-$VER
echo "Entering hottolotto-$VER"
cd ../../../../hottolotto-$VER
echo "moving debian directory to hottolotto-$VER rootdir"
mv deploy/linux/ubuntu/debian .
echo "copying copyright into debian directory"
mv copyright debian
echo "creating archive"
cd ..
tar -czf hottolotto-$VER.tar.gz hottolotto-$VER
echo "**************************"
