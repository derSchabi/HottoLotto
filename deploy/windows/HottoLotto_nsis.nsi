; HottoLotto Installscript for NSIS installer.
; This is used to deploy the program under windows.

;--------------------------------

; The name of the installer
Name "HottoLotto 0.9 BETA"

; The file to write
OutFile "HottoLottoSetup.exe"

; The default installation directory
InstallDir $PROGRAMFILES\HottoLotto

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\HottoLotto" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "HottoLotto (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "HottoLotto.exe"
  File "icudt51.dll"
  File "icuin51.dll"
  File "icuuc51.dll"
  File "libgcc_s_dw2-1.dll"
  File "libstdc++-6.dll"
  File "libwinpthread-1.dll"
  File "Qt5Core.dll"
  File "Qt5Gui.dll"
  File "Qt5Network.dll"
  File "Qt5Widgets.dll"
  File "LICENSE.txt"
  
  SetOutPath $INSTDIR\platforms
  
  File "platforms\qminimal.dll"
  File "platforms\qoffscreen.dll"
  File "platforms\qwindows.dll"
  
  ; Create shortcut on the desktop
  SetOutPath $DESKTOP
  
  CreateShortcut "$DESKTOP\HottoLotto.lnk" $INSTDIR\HottoLotto.exe
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\HottoLotto "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HottoLotto" "DisplayName" "HottoLotto"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HottoLotto" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HottoLotto" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HottoLotto" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\HottoLotto"
  CreateShortCut "$SMPROGRAMS\HottoLotto\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\HottoLotto\HottoLotto.lnk" "$INSTDIR\HottoLotto.exe" "" "$INSTDIR\HottoLotto.exe" 0
  
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HottoLotto"
  DeleteRegKey HKLM SOFTWARE\HottoLotto

  ; Remove files and uninstaller
  Delete $INSTDIR\HottoLotto.exe
  Delete $INSTDIR\icudt51.dll
  Delete $INSTDIR\icuin51.dll
  Delete $INSTDIR\icuuc51.dll
  Delete $INSTDIR\libgcc_s_dw2-1.dll
  Delete $INSTDIR\libstdc++-6.dll
  Delete $INSTDIR\libwinpthread-1.dll
  Delete $INSTDIR\Qt5Core.dll
  Delete $INSTDIR\Qt5Gui.dll
  Delete $INSTDIR\Qt5Network.dll
  Delete $INSTDIR\Qt5Widgets.dll
  Delete $INSTDIR\LICENSE.txt
  
  Delete $INSTDIR\platforms\qminimal.dll
  Delete $INSTDIR\platforms\qoffscreen.dll
  Delete $INSTDIR\platforms\qwindows.dll

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\HottoLotto\*.*"
  
  ; Remove uninstaller
  Delete $INSTDIR\uninstall.exe

  ; Remove directories used
  RMDir "$SMPROGRAMS\HottoLotto"
  RMDir "$INSTDIR\platforms"
  RMDir "$INSTDIR"

SectionEnd
