<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ControlWindow</name>
    <message>
        <location filename="../src/controlWindow.ui" line="32"/>
        <source>ControlWindow</source>
        <translation>ControlWindow</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="44"/>
        <source>Coil Display</source>
        <translation>Coil Display</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="50"/>
        <location filename="../src/controlWindow.ui" line="57"/>
        <location filename="../src/controlWindow.ui" line="64"/>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="71"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="81"/>
        <source>Next Round</source>
        <translation>Next Round</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="88"/>
        <location filename="../src/controlWindow.ui" line="166"/>
        <location filename="../src/controlWindow.ui" line="169"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="98"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="148"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="174"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="179"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="187"/>
        <source>Fullscreen</source>
        <translation>Fullscreen</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.ui" line="192"/>
        <source>New</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.cpp" line="71"/>
        <source>Info about HottoLotto </source>
        <translation>Info about HottoLotto</translation>
    </message>
    <message>
        <location filename="../src/controlWindow.cpp" line="72"/>
        <source>&lt;p&gt;&lt;b&gt;HottoLotto</source>
        <translation>&lt;p&gt;&lt;b&gt;HottoLotto </translation>
    </message>
    <message>
        <location filename="../src/controlWindow.cpp" line="73"/>
        <source>&lt;/b&gt; is a lotto/bingo program for the use of public lotto/bingo events. It is written in C++ using the Qt5 framework.&lt;/p&gt;&lt;p&gt;If you find any bugs want any features or have questions donÂ´t mind writting a mail to &lt;a href=&quot;mailto:chris.schabesberger@gmail.com&quot;&gt;chris.schabesberger@gmail.com&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Copyright &amp;copy; 2013 Christian Schabesberger &amp;lt;chris.schabesberger@gmail.com&amp;gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;You should have received a copy of the GNU General Public License along with this program. If not, see &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;-----------&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt-project.org&quot;&gt;Qt-Project&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;Qt-Licencing&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.randomnumbers.info/&quot;&gt;randomnumbers.info&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Source Code: &lt;a href=&quot;http://github.com/theScrabi/HottoLotto&quot;&gt;http://github.com/theScrabi/HottoLotto&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;/b&gt; is a lotto/bingo program for the use of public lotto/bingo events. It is written in C++ using the Qt5 framework.&lt;/p&gt;&lt;p&gt;If you find any bugs want any features or have questions don´t mind writing a mail to &lt;a href=&quot;mailto:chris.schabesberger@gmail.com&quot;&gt;chris.schabesberger@gmail.com&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Copyright &amp;copy; 2013 Christian Schabesberger &amp;lt;chris.schabesberger@gmail.com&amp;gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;You should have received a copy of the GNU General Public License along with this program. If not, see &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;-----------&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt-project.org&quot;&gt;Qt-Project&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;Qt-Licencing&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.randomnumbers.info/&quot;&gt;randomnumbers.info&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Source Code: &lt;a href=&quot;http://github.com/theScrabi/HottoLotto&quot;&gt;http://github.com/theScrabi/HottoLotto&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;HottoLotto&lt;/b&gt; is a lotto/bingo program for the use of public lotto/bingo events. It is written in C++ using the Qt5 framework.&lt;/p&gt;&lt;p&gt;If you find any bugs want any features or have questions donÂ´t mind writting a mail to &lt;a href=&quot;mailto:chris.schabesberger@gmail.com&quot;&gt;chris.schabesberger@gmail.com&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Copyright &amp;copy; 2013 Christian Schabesberger &amp;lt;chris.schabesberger@gmail.com&amp;gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;You should have received a copy of the GNU General Public License along with this program. If not, see &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;-----------&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt-project.org&quot;&gt;Qt-Project&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;Qt-Licencing&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.randomnumbers.info/&quot;&gt;randomnumbers.info&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Source Code: &lt;a href=&quot;http://github.com/theScrabi/HottoLotto&quot;&gt;http://github.com/theScrabi/HottoLotto&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;HottoLotto&lt;/b&gt; is a lotto/bingo program for the use of public lotto/bingo events. It is written in C++ using the Qt5 framework.&lt;/p&gt;&lt;p&gt;If you find any bugs want any features or have questions donÂ´t mind writting a mail to &lt;a href=&quot;mailto:chris.schabesberger@gmail.com&quot;&gt;chris.schabesberger@gmail.com&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Copyright &amp;copy; 2013 Christian Schabesberger &amp;lt;chris.schabesberger@gmail.com&amp;gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;You should have received a copy of the GNU General Public License along with this program. If not, see &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0&quot;&gt;http://www.gnu.org/licenses&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;-----------&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt-project.org&quot;&gt;Qt-Project&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://qt.digia.com/Product/Licensing/&quot;&gt;Qt-Licencing&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.randomnumbers.info/&quot;&gt;randomnumbers.info&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Source Code: &lt;a href=&quot;http://github.com/theScrabi/HottoLotto&quot;&gt;http://github.com/theScrabi/HottoLotto&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core.cpp" line="130"/>
        <location filename="../src/core.cpp" line="151"/>
        <source>Round</source>
        <translation>Round</translation>
    </message>
</context>
<context>
    <name>Manually</name>
    <message>
        <location filename="../src/generator/manually/manually.ui" line="26"/>
        <source>Manually</source>
        <translation>Manually</translation>
    </message>
    <message>
        <location filename="../src/generator/manually/manually.ui" line="51"/>
        <source>Round 1:</source>
        <translation>Round 1:</translation>
    </message>
    <message>
        <location filename="../src/generator/manually/manually.ui" line="90"/>
        <source>Round 2:</source>
        <translation>Round 2:</translation>
    </message>
    <message>
        <location filename="../src/generator/manually/manually.ui" line="129"/>
        <source>Round 3:</source>
        <translation>Round 3:</translation>
    </message>
    <message>
        <location filename="../src/generator/manually/manually.ui" line="170"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="40"/>
        <source>Connecting www.randomnumbers.info</source>
        <translation>Connecting www.randomnumbers.info</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="41"/>
        <source>Connecting to www.randomnumbers.info</source>
        <translation>Connecting to www.randomnumbers.info</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="54"/>
        <source>Sending Request</source>
        <translation>Sending Request</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="61"/>
        <source>Waiting for Reply</source>
        <translation>Waiting for Reply</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="85"/>
        <source>Couldn`t reach server</source>
        <translation>Couldn`t reach server</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="86"/>
        <source>Using /dev/urandom instead</source>
        <translation>Using /dev/urandom instead</translation>
    </message>
    <message>
        <location filename="../src/generator/qRandom.cpp" line="88"/>
        <source>Using Windows crypt API instead</source>
        <translation>Using Windows crypt API instead</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="35"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="50"/>
        <source>close without saving</source>
        <translation>close without saving</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="53"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="60"/>
        <source>load default settings</source>
        <translation>load default settings</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="63"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="83"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="92"/>
        <location filename="../src/settings/settingsdialog.ui" line="369"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="123"/>
        <source>Random Settings:</source>
        <translation>Random Settings:</translation>
    </message>
    <message utf8="true">
        <location filename="../src/settings/settingsdialog.ui" line="130"/>
        <source>Saves all appearing combination to a File
so they don´t appear twice.</source>
        <translation>Saves all appearing combination to a File
so they don´t appear twice.</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="134"/>
        <source>Combinations once ever</source>
        <translation>Combinations once ever</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="141"/>
        <source>Reset File witch saves the combinations.</source>
        <translation>Reset File witch saves the combinations.</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="144"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="151"/>
        <source>A certain number will only appear
once each game.</source>
        <translation>A certain number will only appear
once each game.</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="155"/>
        <source>Numbers only once per game</source>
        <translation>Numbers only once per game</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="162"/>
        <source>postprocess random with C random</source>
        <translation>postprocess random with C random</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="165"/>
        <source>Post process with srand()</source>
        <translation>Post process with srand()</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="175"/>
        <source>Generator:</source>
        <translation>Generator:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="181"/>
        <source>using /dev/urandom device to generate numbers</source>
        <translation>using /dev/urandom device to generate numbers</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="184"/>
        <source>/dev/urandom</source>
        <translation>/dev/urandom</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="191"/>
        <source>Downloading numbers from www.randomnumbers.info.

(Unsave)</source>
        <translation>Downloading numbers from www.randomnumbers.info.

(Unsave)</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="196"/>
        <source>Quantum Random (Networking)</source>
        <translation>Quantum Random (Networking)</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="203"/>
        <source>using C random with unix time since epoch as input</source>
        <translation>using C random with unix time since epoch as input</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="206"/>
        <source>srand(uTime)</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="213"/>
        <source>a window will pop up witch will let you set the numbers manually</source>
        <translation>a window will pop up witch will let you set the numbers manually</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="216"/>
        <source>Manually</source>
        <translation>Manually</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="239"/>
        <source>!!! Program will reset after clicking OK !!!</source>
        <translation>!!! Program will reset after clicking OK !!!</translation>
    </message>
    <message>
        <source>!!! Settings will be available after programs restart !!!</source>
        <translation type="obsolete">!!! Settings will be available after programs restart !!!</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="257"/>
        <location filename="../src/settings/settingsdialog.ui" line="260"/>
        <source>Start in Fullscreen mode</source>
        <translation>Start in Fullscreen mode</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="267"/>
        <location filename="../src/settings/settingsdialog.ui" line="289"/>
        <source>Speed in pixels per second</source>
        <translation>Speed in pixels per second</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="270"/>
        <source>Speed in px/s:</source>
        <translation>Speed in px/s:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="302"/>
        <location filename="../src/settings/settingsdialog.ui" line="312"/>
        <source>time witch the coils spin around in milliseconds</source>
        <translation>time witch the coils spin around in milliseconds</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="305"/>
        <source>SpinTime in ms:</source>
        <translation>SpinTime in ms:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="322"/>
        <location filename="../src/settings/settingsdialog.ui" line="332"/>
        <source>time witch the coils nead to speed up</source>
        <translation>time witch the coils nead to speed up</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="325"/>
        <source>Speed up Time in ms:</source>
        <translation>Speed up Time in ms:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="342"/>
        <source>General Settings:</source>
        <translation>General Settings:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsdialog.ui" line="372"/>
        <source>Language:</source>
        <translation>Language:</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsDialog.cpp" line="37"/>
        <location filename="../src/settings/settingsDialog.cpp" line="171"/>
        <source>Windows crypt API</source>
        <translation>Windows crypt API</translation>
    </message>
    <message>
        <location filename="../src/settings/settingsDialog.cpp" line="38"/>
        <location filename="../src/settings/settingsDialog.cpp" line="172"/>
        <source>using Windows crypt API to generate random numbers</source>
        <translation>using Windows crypt API to generate random numbers</translation>
    </message>
</context>
</TS>
